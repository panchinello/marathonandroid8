import kotlinx.coroutines.*

fun taskFirst() = runBlocking {
    launch {
        delay(1000L)
        println("World")
    }
    delay(2000L)
    println("Hello,")
}