import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(3) { i ->
                println("I'm sleeping $i ...")
            }
            println("main: I'm tired of waiting!")
        } finally {
            println("I'm running finally")
        }
        println("main: Now I can quit.")
    }
}
