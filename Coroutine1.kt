import kotlinx.coroutines.*

suspend fun firstInt():Int {
    delay(2000L)
    return 4
}
suspend fun secondInt():Int {
    delay(8000L)
    return 12
}

fun taskSecond() = runBlocking {
    val startFirstExperiment = System.currentTimeMillis()
    val firstInt = firstInt()
    val secondInt = secondInt()
    println("${firstInt + secondInt}, ${System.currentTimeMillis() - startFirstExperiment}")

    val startSecondExperiment = System.currentTimeMillis()
    val anotherFirstInt = async { firstInt() }
    val anotherSecondInt = async { secondInt() }
    println("${anotherFirstInt.await() + anotherSecondInt.await()}, ${System.currentTimeMillis() - startSecondExperiment}")
}

/*
* В консоле получаем следующее:
* 16, 10011
* 16, 8023
* Разница во времени объясняется тем, что в 1 эксперименте вторая функция начинает
* работу только после выполнения первой, во втором же эксперименте обе функции
* выполняются одновременно, а значит время работы будет соответствовать функции,
* работающей дольше всего
*/